// BASE SETUP
// ===========================

// CALL THE PACKAGES ---------------------
var express 		= require('express');
var	app     		= express();
var	bodyParser 		= require('body-parser');
var	morgan 			= require('morgan');
var	mongoose		= require('mongoose');
var	jwt				= require('jsonwebtoken');
var	User 			= require('./app/models/user');
var	config 			= require('./config')
var path			= require('path');

//APP CONFIGURATION
// use body parser so we can grab information from POST request
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// configure our app to handle CORS requests
app.use(function(req,res, next) {
	res.setHeader('Access-Control-Allow-Origin', '*');
	res.setHeader('Access-Control-Allow-Methods', 'GET, POST');
	res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type, \Authorization');
	next();
});

//log all requests to the console
app.use(morgan('dev'));

//connect to the database
mongoose.connect(config.database);

// set static file location
// used for requests that our frontend will make
app.use(express.static(__dirname + '/public'));

// API ROUTES ------------------------------
var apiRouter = require('./app/routes/api')(app, express);
app.use('/api', apiRouter);

// main catchall route
// send users to frontend
// has to be registered after API routes
app.get('*', function (req, res) {
	res.sendFile(path.join(__dirname + '/public/app/views/index.html'));
});

// START THE SERVER
// ====================
app.listen(config.port);
console.log('Magic happens on port ' + config.port);