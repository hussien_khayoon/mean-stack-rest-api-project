var mongoose = require('mongoose');
	Schema   = mongoose.Schema;
	bcrypt 	 = require('bcrypt-nodejs');

//user Schema
var UserSchema = new Schema({
	name: String,
	username: {type: String, required: true, index: {unique: true}},
	password: {type: String, required: true, select: false}
});

// has the password before the user is saved
UserSchema.pre('save', function(next) {
	var user = this;

	// has the passowrd only if the password has been chagned or user is new
	if (!user.isModified('password')) return next();

	// generate the hash
	bcrypt.hash(user.password, null, null, function(err, hash) {
		if (err) return next(err);

		//chagne the password to the hashed version
		user.password = hash;
		next();
	});
});

// method to copare a given password with the database hash
UserSchema.methods.comparePassword = function(password) {
	var user = this;

	return bcrypt.compareSync(password, user.password);
};

// return the model
module.exports = mongoose.model('User', UserSchema);